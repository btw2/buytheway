import { atom } from "recoil"
import { getLoginStatus } from "../apollo/btwCache"

export const isLoggedIn = atom<boolean>({
  key: "isLoggedIn",
  default: getLoginStatus(),
})
export const me = atom<any>({ key: "me", default: null })
