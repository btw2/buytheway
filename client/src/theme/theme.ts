import { createMuiTheme } from "@material-ui/core"
import { pink, purple } from "@material-ui/core/colors"

const theme = createMuiTheme({
  palette: {
    primary: purple,
    secondary: pink,
  },
})

export { theme }
