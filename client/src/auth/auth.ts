export const token = "token"

export const logout = () => {
  localStorage.removeItem(token)
}
