import { InMemoryCache, makeVar } from "@apollo/client"

export const btwCache = new InMemoryCache({
  typePolicies: {
    Query: {
      fields: {
        isLoggedIn: {
          read() {
            return isLoggedInVar()
          },
        },
        me: {
          read() {
            return meVar()
          },
        },
      },
    },
  },
})

export const getLoginStatus = () => {
  const token = localStorage.getItem("token")
  if (token) return true
  return false
}

export const isLoggedInVar = makeVar(getLoginStatus())
export const meVar = makeVar<any>(null)
