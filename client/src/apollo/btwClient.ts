import {
  ApolloClient,
  ApolloLink,
  HttpLink,
  InMemoryCache,
} from "@apollo/client"

const endpointURL = process.env.REACT_APP_ENDPOINTURL

const authLink = new ApolloLink((operation, forward) => {
  const token = localStorage.getItem("token")
  if (token) {
    operation.setContext({
      headers: {
        authorization: "Bearer " + token,
      },
    })
  }
  return forward(operation)
})

export const client = new ApolloClient({
  link: ApolloLink.from([authLink, new HttpLink({ uri: endpointURL })]),
  cache: new InMemoryCache(),
})
