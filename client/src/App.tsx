import { AppHead } from "./components/AppHead_recoil"
import { BrowserRouter, Switch, Route } from "react-router-dom"
import React from "react"
import { BigFace } from "./components/BigFace_recoil"

interface Props {}

export const App = (props: Props) => {
  return (
    <>
      <BrowserRouter>
        <AppHead></AppHead>
        <Switch>
          <Route path="/login" exact>
            <div>You are at login page</div>
          </Route>
          <Route path="/">
            <>
              <BigFace />
            </>
          </Route>
        </Switch>
      </BrowserRouter>
    </>
  )
}
