import { useApolloClient, useQuery, useReactiveVar } from "@apollo/client"
import {
  createStyles,
  makeStyles,
  Theme,
  AppBar,
  Toolbar,
  IconButton,
  Typography,
  Button,
} from "@material-ui/core"
import MenuIcon from "@material-ui/icons/Menu"
import React, { useEffect } from "react"
import { isLoggedIn, me } from "../recoil/atoms"
import { logout } from "../auth/auth"
import { currentUserQuery } from "../queries/basic"
import { useRecoilState } from "recoil"

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      flexGrow: 1,
    },
    menuButton: {
      marginRight: theme.spacing(2),
    },
    title: {
      flexGrow: 1,
    },
  })
)
interface Props {}

export const AppHead = (props: Props) => {
  const classes = useStyles()
  const [loggedState, setLoggedState] = useRecoilState(isLoggedIn)
  const [iam, setIam] = useRecoilState(me)
  const { data, error, loading, client, refetch } = useQuery(currentUserQuery)

  if (error) {
    localStorage.removeItem("token")
  }

  useEffect(() => {
    if (data?.currentUser && data.currentUser.username && !error) {
      setLoggedState(true)
      setIam(data.currentUser)
    } else {
      setLoggedState(false)
      setIam(null)
    }
    return () => {}
  }, [data])

  const onClickLogout = (event: any) => {
    event.preventDefault()
    logout()
    setLoggedState(false)
    setIam(null)
  }

  const showButtons = () => {
    if (iam && iam.username) {
      return (
        <div>
          <Button color="inherit">{iam.username}</Button>
          <Button color="inherit" onClick={onClickLogout}>
            Logout
          </Button>
        </div>
      )
    } else {
      return <Button color="inherit">Login</Button>
    }
  }

  return (
    <AppBar position="static">
      <Toolbar>
        <IconButton
          edge="start"
          className={classes.menuButton}
          color="inherit"
          aria-label="menu"
        >
          <MenuIcon />
        </IconButton>
        <Typography variant="h6" className={classes.title} component="div">
          Buy the way
        </Typography>
        {showButtons()}
      </Toolbar>
    </AppBar>
  )
}
