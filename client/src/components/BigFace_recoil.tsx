import { useRecoilValue } from "recoil"
import { Helmet } from "react-helmet"
import { me } from "../recoil/atoms"

interface Props {}

export const BigFace = (props: Props) => {
  const iam = useRecoilValue(me)
  const text = () => {
    if (iam) {
      return iam.username
    } else {
      return "idiot"
    }
  }

  return (
    <>
      <Helmet>
        <title>Big Face</title>
      </Helmet>
      <div>
        <p>{text()}</p>
      </div>
    </>
  )
}
