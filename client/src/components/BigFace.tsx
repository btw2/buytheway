import { useReactiveVar } from "@apollo/client"
import { meVar } from "../apollo/btwCache"

interface Props {}

export const BigFace = () => {
  const me = useReactiveVar(meVar)
  const text = () => {
    if (me) {
      return me.username
    } else {
      return "idiot"
    }
  }

  return (
    <div>
      <p>{text()}</p>
    </div>
  )
}
