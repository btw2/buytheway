import { useQuery, useReactiveVar } from "@apollo/client"
import {
  createStyles,
  makeStyles,
  Theme,
  AppBar,
  Toolbar,
  IconButton,
  Typography,
  Button,
} from "@material-ui/core"
import MenuIcon from "@material-ui/icons/Menu"
import React, { useEffect } from "react"
import { isLoggedInVar, meVar } from "../apollo/btwCache"
import { logout } from "../auth/auth"
import { currentUserQuery } from "../queries/basic"

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      flexGrow: 1,
    },
    menuButton: {
      marginRight: theme.spacing(2),
    },
    title: {
      flexGrow: 1,
    },
  })
)
interface Props {}

export const AppHead = (props: Props) => {
  const classes = useStyles()
  const { data, error, loading, client, refetch } = useQuery(currentUserQuery)

  useEffect(() => {
    if (data?.currentUser?.username) {
      isLoggedInVar(true)
      meVar(data.currentUser)
    } else {
      isLoggedInVar(false)
      meVar(null)
    }
  }, [data])

  const onClickLogout = (event: any) => {
    event.preventDefault()
    logout()
    isLoggedInVar(false)
    meVar(null)
  }

  const me = useReactiveVar(meVar)
  const showButtons = () => {
    if (me) {
      return (
        <div>
          <Button color="inherit">{me.username}</Button>
          <Button color="inherit" onClick={onClickLogout}>
            Logout
          </Button>
        </div>
      )
    } else {
      return <Button color="inherit">Login</Button>
    }
  }

  return (
    <AppBar position="static">
      <Toolbar>
        <IconButton
          edge="start"
          className={classes.menuButton}
          color="inherit"
          aria-label="menu"
        >
          <MenuIcon />
        </IconButton>
        <Typography variant="h6" className={classes.title} component="div">
          Buy the way
        </Typography>
        {showButtons()}
      </Toolbar>
    </AppBar>
  )
}
