"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const type_graphql_1 = require("type-graphql");
const path_1 = __importDefault(require("path"));
const AuthResolver_1 = require("../resolvers/AuthResolver");
const custom_auth_checker_1 = require("../auth/custom-auth-checker");
async function createSchema() {
    const schema = await type_graphql_1.buildSchema({
        resolvers: [AuthResolver_1.AuthResolver],
        authChecker: custom_auth_checker_1.customAuthChecker,
        emitSchemaFile: path_1.default.resolve(__dirname, "../../schema.graphql"),
        validate: false,
    });
    return schema;
}
exports.default = createSchema;
