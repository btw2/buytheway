"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.config = void 0;
const dotenv_1 = __importDefault(require("dotenv"));
dotenv_1.default.config();
exports.config = {
    server: { port: process.env.PORT || 9000, host: process.env.HOST },
    mongodb: {
        path: process.env.MONGODB_PATH || "mongodb://localhost:27017/btw",
        test_path: process.env.MONGODB_TEST_PATH || "mongodb://localhost:27017/btw_test",
    },
    jwt: {
        secrect: Buffer.from(process.env.JWT_SECRET || "nothing", "base64"),
        verification: Buffer.from(process.env.JWT_VERIFICATION || "nothing", "base64"),
        forgetPassword: Buffer.from(process.env.JWT_FORGETPASSWORD || "nothing", "base64"),
    },
    mail: {
        admin: {
            host: process.env.ADMIN_HOST,
            port: process.env.ADMIN_PORT,
            secure: true,
            auth: {
                user: process.env.ADMIN_MAIL_USER,
                pass: process.env.ADMIN_MAIL_PASS,
            },
        },
    },
    verifyServer: {
        path: `http://${process.env.HOST}:${process.env.PORT}/users/verify`,
    },
    forgetPassword: {
        path: `http://${process.env.HOST}:${process.env
            .PORT}/users/forgetPassword`,
    },
};
