"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.findUser = exports.customAuthChecker = void 0;
const User_1 = require("../entities/User");
const customAuthChecker = async ({ context: { user } }, roles) => {
    const docUser = await User_1.UserModel.findById(user.id);
    if (roles.length === 0) {
        return docUser !== undefined;
    }
    if (!docUser)
        return false;
    if (docUser.roles.some((role) => roles.includes(role))) {
        return true;
    }
    return false;
};
exports.customAuthChecker = customAuthChecker;
const findUser = async function (req, res, next) {
    if (req.user) {
        const docUser = await User_1.UserModel.findById(req.user.id, "-password -_id");
        if (docUser) {
            ;
            req.user = { ...req.user, ...docUser.toObject() };
        }
        else {
            ;
            req.user = undefined;
        }
    }
    next();
};
exports.findUser = findUser;
