"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AuthResolver = exports.signUserToken = void 0;
const type_graphql_1 = require("type-graphql");
const apollo_server_express_1 = require("apollo-server-express");
const jsonwebtoken_1 = __importDefault(require("jsonwebtoken"));
const config_1 = require("../config");
const User_1 = require("../entities/User");
const types_1 = require("../types");
const chalk_1 = __importDefault(require("chalk"));
const EditUserInput_1 = require("../types/EditUserInput");
const signUserToken = (user) => {
    const payload = {
        id: user.id,
        username: user.username,
        email: user.email,
    };
    const token = jsonwebtoken_1.default.sign(payload, config_1.config.jwt.secrect, { algorithm: "HS256" });
    return token;
};
exports.signUserToken = signUserToken;
let AuthResolver = class AuthResolver {
    async register({ username, email, password }) {
        const existingUsername = await User_1.UserModel.findOne({ username });
        if (existingUsername) {
            throw new apollo_server_express_1.ApolloError(`username: ${username} is already exist`);
        }
        const existingEmail = await User_1.UserModel.findOne({ email });
        if (existingEmail) {
            throw new apollo_server_express_1.ApolloError(`email: ${email} is already exist`);
        }
        const user = new User_1.UserModel({ username, email, password });
        await user.save();
        const token = exports.signUserToken(user);
        return {
            user,
            token,
        };
    }
    async login(email, password) {
        try {
            const user = await User_1.UserModel.findOne({ email });
            if (!user) {
                throw new apollo_server_express_1.ApolloError(`${email} can not be found`);
            }
            const logged = await user.comparePassword(password);
            if (!logged) {
                throw new apollo_server_express_1.ApolloError(`password is wrong`);
            }
            const token = exports.signUserToken(user);
            return { user, token };
        }
        catch (error) {
            throw error;
        }
    }
    async deleteUser(userId, currentUser) {
        if (userId == currentUser.id)
            return null;
        const res = await User_1.UserModel.findByIdAndRemove(userId);
        return res;
    }
    async editUserProfile(input, currentUser) {
        if (input.id !== currentUser.id) {
            throw new apollo_server_express_1.ApolloError("you are not allowed to change other users profile");
        }
        const userId = input.id;
        const newInput = { ...input };
        delete newInput.id;
        if (newInput.username == currentUser.username) {
            delete newInput.username;
        }
        if (newInput.email == currentUser.email) {
            delete newInput.email;
        }
        if (Object.keys(newInput).length === 0) {
            throw new apollo_server_express_1.ApolloError("nothing to change");
        }
        if (newInput.username && newInput.username !== "") {
            const res = await User_1.UserModel.findOne({ username: newInput.username });
            if (res) {
                throw new apollo_server_express_1.ApolloError(`username: ${newInput.username} already existiert`);
            }
        }
        if (newInput.email && newInput.email !== "") {
            const res = await User_1.UserModel.findOne({ email: newInput.email });
            if (res) {
                throw new apollo_server_express_1.ApolloError(`email: ${newInput.email} already existiert`);
            }
        }
        const user = await User_1.UserModel.findByIdAndUpdate(userId, newInput);
        return user;
    }
    async editUserPassword(userId, password, currentUser) {
        if (currentUser.id !== userId) {
            throw new apollo_server_express_1.ApolloError("You are not allowed to change other's password!");
        }
        if (password == "") {
            throw new apollo_server_express_1.ApolloError("Please give the new password, empty is not allowed!");
        }
        const user = await User_1.UserModel.findByIdAndUpdate(userId, {
            password: await User_1.UserModel.hashPassword(password),
        });
        return user;
    }
    hello(user) {
        console.log(chalk_1.default.blue(JSON.stringify(user)));
        return "aaa";
    }
    async currentUser(ctx) {
        if (!ctx.req.user) {
            return null;
        }
        const user = await User_1.UserModel.findById(ctx.req.user.id);
        if (user) {
            return user;
        }
        else {
            return null;
        }
    }
};
__decorate([
    type_graphql_1.Mutation(() => types_1.UserResponse),
    __param(0, type_graphql_1.Arg("input")),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [types_1.AuthInput]),
    __metadata("design:returntype", Promise)
], AuthResolver.prototype, "register", null);
__decorate([
    type_graphql_1.Mutation(() => types_1.UserResponse),
    __param(0, type_graphql_1.Arg("email")),
    __param(1, type_graphql_1.Arg("password")),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, String]),
    __metadata("design:returntype", Promise)
], AuthResolver.prototype, "login", null);
__decorate([
    type_graphql_1.Authorized("admin"),
    type_graphql_1.Mutation(() => User_1.User, { nullable: true }),
    __param(0, type_graphql_1.Arg("userId")),
    __param(1, type_graphql_1.Ctx("user")),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, Object]),
    __metadata("design:returntype", Promise)
], AuthResolver.prototype, "deleteUser", null);
__decorate([
    type_graphql_1.Authorized(),
    type_graphql_1.Mutation(() => User_1.User, { nullable: true }),
    __param(0, type_graphql_1.Arg("input")),
    __param(1, type_graphql_1.Ctx("user")),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [EditUserInput_1.EditUserProfileInput, Object]),
    __metadata("design:returntype", Promise)
], AuthResolver.prototype, "editUserProfile", null);
__decorate([
    type_graphql_1.Authorized(),
    type_graphql_1.Mutation((type) => User_1.User),
    __param(0, type_graphql_1.Arg("userId")),
    __param(1, type_graphql_1.Arg("password")),
    __param(2, type_graphql_1.Ctx("user")),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, String, Object]),
    __metadata("design:returntype", Promise)
], AuthResolver.prototype, "editUserPassword", null);
__decorate([
    type_graphql_1.Query(() => String),
    __param(0, type_graphql_1.Ctx("user")),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", void 0)
], AuthResolver.prototype, "hello", null);
__decorate([
    type_graphql_1.Query(() => User_1.User, { nullable: true }),
    __param(0, type_graphql_1.Ctx()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], AuthResolver.prototype, "currentUser", null);
AuthResolver = __decorate([
    type_graphql_1.Resolver()
], AuthResolver);
exports.AuthResolver = AuthResolver;
