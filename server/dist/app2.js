"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
require("reflect-metadata");
const express_1 = __importDefault(require("express"));
const cors_1 = __importDefault(require("cors"));
const express_jwt_1 = __importDefault(require("express-jwt"));
const config_1 = require("./config");
const session_1 = __importDefault(require("./session"));
const schema_1 = __importDefault(require("./schema"));
const apollo_server_express_1 = require("apollo-server-express");
const custom_auth_checker_1 = require("./auth/custom-auth-checker");
const port = config_1.config.server.port;
const host = config_1.config.server.host;
async function createServer() {
    try {
        await session_1.default();
        const app = express_1.default();
        app.use(cors_1.default(), express_1.default.json(), express_jwt_1.default({
            secret: config_1.config.jwt.secrect,
            algorithms: ["HS256"],
            credentialsRequired: false,
        }));
        app.use(custom_auth_checker_1.findUser);
        const schema = await schema_1.default();
        const apolloServer = new apollo_server_express_1.ApolloServer({
            schema,
            context: ({ req }) => {
                const context = { req, user: req.user };
                return context;
            },
            introspection: true,
            playground: {
                settings: {
                    "request.credentials": "include",
                },
            },
        });
        apolloServer.applyMiddleware({ app, path: "/graphql" });
        app.get("/", (req, res) => {
            res.send("hello!");
        });
        app.use(function (err, req, res, next) {
            if (err.name === "UnauthorizedError") {
                res.status(401).send("invalid token...");
            }
        });
        app.listen(config_1.config.server.port, () => {
            console.info(`Server ready at http://${host}:${port}${apolloServer.graphqlPath}`);
        });
    }
    catch (error) {
        console.log(error);
    }
}
createServer();
