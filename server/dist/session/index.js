"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
const config_1 = require("../config");
async function createSession() {
    const MONGO_URL = config_1.config.mongodb.path;
    if (!MONGO_URL) {
        throw new Error("Missing MONGO_URL");
    }
    const options = {
        useNewUrlParser: true,
        useCreateIndex: true,
        useFindAndModify: false,
        useUnifiedTopology: true,
    };
    await mongoose_1.connect(MONGO_URL, options);
}
exports.default = createSession;
