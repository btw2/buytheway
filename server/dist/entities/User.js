"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var User_1;
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserModel = exports.User = exports.ERole = void 0;
const typegoose_1 = require("@typegoose/typegoose");
const type_graphql_1 = require("type-graphql");
const type_graphql_2 = require("type-graphql");
const bcrypt_1 = __importDefault(require("bcrypt"));
var ERole;
(function (ERole) {
    ERole["ADMIN"] = "admin";
    ERole["USER"] = "user";
    ERole["BUSINESS"] = "business";
})(ERole = exports.ERole || (exports.ERole = {}));
let User = User_1 = class User {
    static async hashPassword(originalPassword) {
        try {
            const hashedPassword = await bcrypt_1.default.hash(originalPassword, 12);
            return hashedPassword;
        }
        catch (error) {
            throw error;
        }
    }
    async comparePassword(candidatePassword) {
        try {
            return await bcrypt_1.default.compare(candidatePassword, this.password);
        }
        catch (error) {
            throw error;
        }
    }
};
__decorate([
    type_graphql_1.Field((type) => type_graphql_2.ID),
    __metadata("design:type", String)
], User.prototype, "id", void 0);
__decorate([
    type_graphql_1.Field((type) => [String]),
    typegoose_1.prop({ default: [ERole.USER], type: () => [String] }),
    __metadata("design:type", Array)
], User.prototype, "roles", void 0);
__decorate([
    type_graphql_1.Field(),
    typegoose_1.prop({ required: true }),
    __metadata("design:type", String)
], User.prototype, "username", void 0);
__decorate([
    type_graphql_1.Field(),
    typegoose_1.prop({ required: true }),
    __metadata("design:type", String)
], User.prototype, "email", void 0);
__decorate([
    typegoose_1.prop({ required: true }),
    __metadata("design:type", String)
], User.prototype, "password", void 0);
__decorate([
    type_graphql_1.Field((type) => String),
    typegoose_1.prop({ default: Date.now }),
    __metadata("design:type", Date)
], User.prototype, "createdAt", void 0);
__decorate([
    typegoose_1.prop(),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], User, "hashPassword", null);
User = User_1 = __decorate([
    typegoose_1.pre("save", async function (next) {
        let user = this;
        if (!user.isModified("password")) {
            return next();
        }
        try {
            user.password = await User_1.hashPassword(user.password);
            return next();
        }
        catch (error) {
            throw error;
        }
    }),
    type_graphql_1.ObjectType(),
    typegoose_1.modelOptions({ schemaOptions: { timestamps: true } })
], User);
exports.User = User;
exports.UserModel = typegoose_1.getModelForClass(User);
