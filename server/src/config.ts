import { prop } from "@typegoose/typegoose"
import dotenv from "dotenv"

dotenv.config()

export const config = {
  server: { port: process.env.PORT || 9000, host: process.env.HOST },
  mongodb: {
    path: process.env.MONGODB_PATH || "mongodb://localhost:27017/btw",
    test_path:
      process.env.MONGODB_TEST_PATH || "mongodb://localhost:27017/btw_test",
  },
  jwt: {
    secrect: Buffer.from(process.env.JWT_SECRET || "nothing", "base64"),
    verification: Buffer.from(
      process.env.JWT_VERIFICATION || "nothing",
      "base64"
    ),
    forgetPassword: Buffer.from(
      process.env.JWT_FORGETPASSWORD || "nothing",
      "base64"
    ),
  },
  mail: {
    admin: {
      host: process.env.ADMIN_HOST!,
      port: process.env.ADMIN_PORT!,
      secure: true,
      auth: {
        user: process.env.ADMIN_MAIL_USER!,
        pass: process.env.ADMIN_MAIL_PASS!,
      },
    },
  },
  verifyServer: {
    path: `http://${process.env.HOST!}:${process.env.PORT!}/users/verify`,
  },
  forgetPassword: {
    path: `http://${process.env.HOST!}:${process.env
      .PORT!}/users/forgetPassword`,
  },
}
