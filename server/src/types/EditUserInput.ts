import { InputType, Field } from "type-graphql"

@InputType()
export class EditUserProfileInput {
  @Field()
  id: string

  @Field({ nullable: true })
  username: string

  @Field({ nullable: true })
  email: string
}
