import { connect } from "mongoose"
import {config} from '../config'

export default async function createSession() {
  const MONGO_URL = config.mongodb.path
  if (!MONGO_URL) {
    throw new Error("Missing MONGO_URL")
  }

  const options = {
    useNewUrlParser: true,
    useCreateIndex: true,
    useFindAndModify: false,
    useUnifiedTopology: true,
  }

  await connect(MONGO_URL, options)
}