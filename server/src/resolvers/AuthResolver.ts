import { Arg, Mutation, Resolver, Query, Ctx, Authorized } from "type-graphql"
import { ApolloError } from "apollo-server-express"
import jwt from "jsonwebtoken"

import { config } from "../config"
import { User, UserModel } from "../entities/User"
import { AuthInput, UserResponse } from "../types"
import { DocumentType } from "@typegoose/typegoose"
import chalk from "chalk"
import { Context } from "vm"
import { EditUserProfileInput } from "../types/EditUserInput"

export const signUserToken = (user: DocumentType<User>): string => {
  const payload = {
    id: user.id,
    username: user.username,
    email: user.email,
  }

  const token = jwt.sign(payload, config.jwt.secrect, { algorithm: "HS256" })

  return token
}

@Resolver()
export class AuthResolver {
  @Mutation(() => UserResponse)
  async register(
    @Arg("input") { username, email, password }: AuthInput
  ): Promise<UserResponse> {
    const existingUsername = await UserModel.findOne({ username })
    if (existingUsername) {
      throw new ApolloError(`username: ${username} is already exist`)
    }
    const existingEmail = await UserModel.findOne({ email })

    if (existingEmail) {
      throw new ApolloError(`email: ${email} is already exist`)
    }

    const user = new UserModel({ username, email, password })

    await user.save()

    const token = signUserToken(user)

    return {
      user,
      token,
    }
  }

  @Mutation(() => UserResponse)
  async login(
    @Arg("email") email: string,
    @Arg("password") password: string
  ): Promise<UserResponse> {
    try {
      const user = await UserModel.findOne({ email })

      if (!user) {
        throw new ApolloError(`${email} can not be found`)
      }

      const logged = await user.comparePassword(password)

      if (!logged) {
        throw new ApolloError(`password is wrong`)
      }

      const token = signUserToken(user)

      return { user, token }
    } catch (error) {
      throw error
    }
  }

  @Authorized("admin")
  @Mutation(() => User, { nullable: true })
  async deleteUser(
    @Arg("userId") userId: string,
    @Ctx("user") currentUser: any
  ) {
    // You can not delete yourself
    if (userId == currentUser.id) return null

    // delete the user
    const res = await UserModel.findByIdAndRemove(userId)

    return res
  }

  @Authorized()
  @Mutation(() => User, { nullable: true })
  async editUserProfile(
    @Arg("input") input: EditUserProfileInput,
    @Ctx("user") currentUser: any
  ) {
    if (input.id !== currentUser.id) {
      throw new ApolloError("you are not allowed to change other users profile")
    }

    const userId = input.id

    const newInput: any = { ...input }
    delete newInput.id

    // if nothing changes, then you should not update
    if (newInput.username == currentUser.username) {
      delete newInput.username
    }

    if (newInput.email == currentUser.email) {
      delete newInput.email
    }

    if (Object.keys(newInput).length === 0) {
      throw new ApolloError("nothing to change")
    }

    // username should not be already used
    if (newInput.username && newInput.username !== "") {
      const res = await UserModel.findOne({ username: newInput.username })

      if (res) {
        throw new ApolloError(
          `username: ${newInput.username} already existiert`
        )
      }
    }

    // email should not be already used
    if (newInput.email && newInput.email !== "") {
      const res = await UserModel.findOne({ email: newInput.email })

      if (res) {
        throw new ApolloError(`email: ${newInput.email} already existiert`)
      }
    }

    const user = await UserModel.findByIdAndUpdate(userId, newInput)

    return user
  }

  @Authorized()
  @Mutation((type) => User)
  async editUserPassword(
    @Arg("userId") userId: string,
    @Arg("password") password: string,
    @Ctx("user") currentUser: any
  ) {
    if (currentUser.id !== userId) {
      throw new ApolloError("You are not allowed to change other's password!")
    }

    if (password == "") {
      throw new ApolloError(
        "Please give the new password, empty is not allowed!"
      )
    }

    const user = await UserModel.findByIdAndUpdate(userId, {
      password: await UserModel.hashPassword(password),
    })

    return user
  }

  @Query(() => String)
  hello(@Ctx("user") user: any) {
    console.log(chalk.blue(JSON.stringify(user)))
    return "aaa"
  }

  @Query(() => User, { nullable: true })
  async currentUser(@Ctx() ctx: Context): Promise<DocumentType<User> | null> {
    if (!ctx.req.user) {
      return null
    }

    const user = await UserModel.findById(ctx.req.user.id)

    if (user) {
      return user
    } else {
      return null
    }
  }
}
