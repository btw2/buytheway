import "reflect-metadata"
import express, { Request, Response, NextFunction } from "express"
import cors from "cors"
import jwt from "express-jwt"
import { config } from "./config"
import createSession from "./session"
import createSchema from "./schema"
import { ApolloServer } from "apollo-server-express"
import chalk from "chalk"
import { findUser } from "./auth/custom-auth-checker"

const port = config.server.port
const host = config.server.host

async function createServer() {
  try {
    await createSession()
    const app = express()
    app.use(
      cors(),
      express.json(),
      jwt({
        secret: config.jwt.secrect,
        algorithms: ["HS256"],
        credentialsRequired: false,
      }),
    )

    app.use(findUser)

    const schema = await createSchema()

    const apolloServer = new ApolloServer({
      schema,
      context: ({ req }) => {
        const context = { req, user: (req as any).user }
        return context
      },
      introspection: true,
      playground: {
        settings: {
          "request.credentials": "include",
        },
      },
    })

    apolloServer.applyMiddleware({ app, path: "/graphql" })

    app.get("/", (req: Request, res) => {
      res.send("hello!")
    })

    app.use(function (
      err: Error,
      req: Request,
      res: Response,
      next: NextFunction
    ) {
      if (err.name === "UnauthorizedError") {
        res.status(401).send("invalid token...")
      }
    })

    app.listen(config.server.port, () => {
      console.info(
        `Server ready at http://${host}:${port}${apolloServer.graphqlPath}`
      )
    })
  } catch (error) {
    console.log(error)
  }
}

createServer()
