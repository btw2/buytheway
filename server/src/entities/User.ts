import {
  prop,
  getModelForClass,
  pre,
  modelOptions,
  DocumentType,
} from "@typegoose/typegoose"
import { Field, ObjectType } from "type-graphql"
import { ID } from "type-graphql"
import bcryt from "bcrypt"

export enum ERole {
  ADMIN = "admin",
  USER = "user",
  BUSINESS = "business",
}

@pre<User>("save", async function (next) {
  let user = this

  if (!user.isModified("password")) {
    return next()
  }

  try {
    user.password = await User.hashPassword(user.password)
    return next()
  } catch (error) {
    throw error
  }
})
@ObjectType()
@modelOptions({ schemaOptions: { timestamps: true } })
export class User {
  @Field((type) => ID)
  id: string

  @Field((type) => [String])
  @prop({ default: [ERole.USER], type: () => [String] })
  public roles!: ERole[]

  @Field()
  @prop({ required: true })
  public username!: string

  @Field()
  @prop({ required: true })
  public email!: string

  @prop({ required: true })
  public password!: string

  @Field((type) => String)
  @prop({ default: Date.now })
  public createdAt?: Date

  @prop()
  public static async hashPassword(originalPassword: string) {
    try {
      const hashedPassword = await bcryt.hash(originalPassword, 12)
      return hashedPassword
    } catch (error) {
      throw error
    }
  }

  public async comparePassword(
    this: DocumentType<User>,
    candidatePassword: string
  ) {
    try {
      return await bcryt.compare(candidatePassword, this.password)
    } catch (error) {
      throw error
    }
  }
}

export const UserModel = getModelForClass(User)
