import chalk from "chalk"
import { RequestHandler } from "express"
import { AuthChecker } from "type-graphql"
import { UserModel } from "../entities/User"

export const customAuthChecker: AuthChecker<any> = async (
  { context: { user } },
  roles
) => {
  // here we can read the user from context
  // and check his permission in the db against the `roles` argument
  // that comes from the `@Authorized` decorator, eg. ["ADMIN", "MODERATOR"]
  const docUser = await UserModel.findById(user.id)

  if (roles.length === 0) {
    return docUser !== undefined
  }

  if (!docUser) return false

  if (docUser.roles.some((role) => roles.includes(role))) {
    return true
  }

  return false
}

export const findUser: RequestHandler = async function (req, res, next) {
  if ((req as any).user) {
    const docUser = await UserModel.findById(
      (req as any).user.id,
      "-password -_id"
    )
    if (docUser) {
      ;(req as any).user = { ...(req as any).user, ...docUser.toObject() }
    } else {
      ;(req as any).user = undefined
    }
  }

  next()
}
