import { GraphQLSchema } from "graphql"
import { buildSchema } from "type-graphql"
import path from "path"
import { AuthResolver } from "../resolvers/AuthResolver"
import { customAuthChecker } from "../auth/custom-auth-checker"

export default async function createSchema(): Promise<GraphQLSchema> {
  const schema = await buildSchema({
    resolvers: [AuthResolver],
    authChecker: customAuthChecker,
    emitSchemaFile: path.resolve(__dirname, "../../schema.graphql"),
    validate: false,
  })

  return schema
}
